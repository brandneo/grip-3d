import React from 'react';
import styled from 'styled-components'

const StyledButton = styled.button`
    background-color: #000;
    color: #fff;
    font-size: .9rem;
    line-height: 1.1;
    text-transform: uppercase;
    text-decoration: none;
    letter-spacing: 1px;
    border-radius: 25px;
    min-height: 50px;
    display: inline-flex;
    align-items: center;
    justify-content: center;
    padding: 10px 25px 8px 25px;
    transition-duration: .5s;
`

const Button = ({children, onClick, ...props}) => (
    <StyledButton onClick={onClick}>
        {children}
    </StyledButton>
);

export default Button;