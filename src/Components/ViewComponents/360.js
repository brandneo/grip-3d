import React from "react";
import styled from "styled-components";
import RotateImage from "../../assets/images/360.png";

const Styled360 = styled.img`
    position: absolute;
    left: 40vw; 
    top: 28px;
    width: 70%;

    // Extra small devices (portrait phones, less than 576px)
    @media (min-width: 575.98px) { 
        top: -10px;
        width: 25%;
     }

    // Small devices (landscape phones, less than 768px)
    @media (min-width: 767.98px) { 
        top: -10px;
        width: 25%;
        left: 45vw;
     }

    // Medium devices (tablets, less than 992px)
    @media (min-width: 991.98px) { 
        top: 25px;
        width: 25%;
        left: 45vw;
     }

    // Large devices (desktops, less than 1200px)
    @media (min-width: 1199.98px) { 
        top: 25px;
        width: 25%;
        left: 45vw;
     }
`;

const Img = ({ children, ...props }) => <Styled360 src={RotateImage} alt="360">{children}</Styled360>;

export default Img;
