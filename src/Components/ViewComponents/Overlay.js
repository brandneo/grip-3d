import React from "react";
import styled from "styled-components";

const StyledOverlay = styled.div`
    position: absolute;
    z-index: 1;
    bottom: 0;
`;

const Overlay = ({ children, ...props }) => <StyledOverlay>{children}</StyledOverlay>;

export default Overlay;
