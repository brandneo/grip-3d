import React from "react";
import styled from "styled-components";

const Dot = styled.a`
    height: 30px;
    width: 30px;
    margin: 0 5%;
    border-radius: 50%;
    display: inline-block;
`;

const SingleDot = styled.span`
    height: 30px;
    width: 30px;
    padding: 5%;
    margin-top: 20%;
    border-radius: 50%;
    /* Permalink - use to edit and share this gradient: https://colorzilla.com/gradient-editor/#8c8c8c+0,030303+93 */
    background: ${(props) =>
        props.tool === "MGW" && props.color === "#030303" ? "rgb(140,140,140)" : "rgb(0, 0, 0)"}; /* Old browsers */
    background: ${(props) =>
        props.tool === "MGW" && props.color === "#030303"
            ? "-moz-linear-gradient(left,  rgba(140,140,140,1) 0%, rgba(3,3,3,1) 93%)"
            : "rgb(0, 0, 0)"}; /* FF3.6-15 */
    background: ${(props) =>
        props.tool === "MGW" && props.color === "#030303"
            ? "-webkit-linear-gradient(left,  rgba(140,140,140,1) 0%,rgba(3,3,3,1) 93%)"
            : "rgb(0, 0, 0)"}; /* Chrome10-25,Safari5.1-6 */
    background: ${(props) =>
        props.tool === "MGW" && props.color === "#030303"
            ? "linear-gradient(to right,  rgba(140,140,140,1) 0%,rgba(3,3,3,1) 93%)"
            : "rgb(0, 0, 0)"}; /* W3C, IE10+, FF16+, Chrome26+, Opera12+, Safari7+ */
    filter: ${(props) =>
        props.tool === "MGW" && props.color === "#030303"
            ? "progid:DXImageTransform.Microsoft.gradient( startColorstr='#8c8c8c', endColorstr='#030303',GradientType=1 )"
            : "progid:DXImageTransform.Microsoft.gradient( startColorstr='#8c8c8c', endColorstr='#030303',GradientType=1 )"}; /* IE6-9 */

    display: inline-block;
`;

const Top = styled.span`
    height: 15px;
    width: 30px;
    // margin-bottom: -20%;
    margin-bottom: -9px;
    padding: 5%;
    border-radius: 30px 30px 0 0;
    /* Permalink - use to edit and share this gradient: https://colorzilla.com/gradient-editor/#92e9ba+0,2d9e61+100 */
    /* Permalink - use to edit and share this gradient: https://colorzilla.com/gradient-editor/#ffffff+0,ebebeb+50,979797+100 */
    /* Permalink - use to edit and share this gradient: https://colorzilla.com/gradient-editor/#fc7474+0,ab3636+100 */
    background: ${(props) =>
        props.tool === "SHW" && props.color === "#2d9e61"
            ? "rgb(146, 233, 186)"
            : props.tool === "SHW" && props.color === "#979797"
            ? "rgb(255, 255, 255)"
            : props.tool === "MGW" && props.color === "#fc7474"
            ? "rgb(252,116,116)"
            : "rgb(0, 0, 0)"}; /* Old browsers */
    background: ${(props) =>
        props.tool === "SHW" && props.color === "#2d9e61"
            ? "-moz-linear-gradient(left,rgba(146, 233, 186, 1) 0%,rgba(45, 158, 97, 1) 100%)"
            : props.tool === "SHW" && props.color === "#979797"
            ? "-moz-linear-gradient(left,rgba(255, 255, 255, 1) 0%,rgba(235, 235, 235, 1) 50%,rgba(151, 151, 151, 1) 100%)"
            : props.tool === "MGW" && props.color === "#fc7474"
            ? "-moz-linear-gradient(left,  rgba(252,116,116,1) 0%, rgba(171,54,54,1) 100%)"
            : "rgb(0, 0, 0)"}; /* FF3.6-15 */
    background: ${(props) =>
        props.tool === "SHW" && props.color === "#2d9e61"
            ? "-webkit-linear-gradient(left,rgba(146, 233, 186, 1) 0%,rgba(45, 158, 97, 1) 100%)"
            : props.tool === "SHW" && props.color === "#979797"
            ? "-webkit-linear-gradient(left,rgba(255, 255, 255, 1) 0%,rgba(235, 235, 235, 1) 50%,rgba(151, 151, 151, 1) 100%)"
            : props.tool === "MGW" && props.color === "#fc7474"
            ? "-webkit-linear-gradient(left,  rgba(252,116,116,1) 0%,rgba(171,54,54,1) 100%)"
            : "rgb(0, 0, 0)"}; /* Chrome10-25,Safari5.1-6 */
    background: ${(props) =>
        props.tool === "SHW" && props.color === "#2d9e61"
            ? "linear-gradient(to right,rgba(146, 233, 186, 1) 0%,rgba(45, 158, 97, 1) 100%)"
            : props.tool === "SHW" && props.color === "#979797"
            ? "linear-gradient(to right,rgba(255, 255, 255, 1) 0%,rgba(235, 235, 235, 1) 50%,rgba(151, 151, 151, 1) 100%)"
            : props.tool === "MGW" && props.color === "#fc7474"
            ? "linear-gradient(to right,  rgba(252,116,116,1) 0%,rgba(171,54,54,1) 100%)"
            : "rgb(0, 0, 0)"}; /* W3C, IE10+, FF16+, Chrome26+, Opera12+, Safari7+ */
    filter: ${(props) =>
        props.tool === "SHW" && props.color === "#2d9e61"
            ? "progid:DXImageTransform.Microsoft.gradient( startColorstr='#92e9ba', endColorstr='#2d9e61',GradientType=1 )"
            : props.tool === "SHW" && props.color === "#979797"
            ? "progid:DXImageTransform.Microsoft.gradient( startColorstr='#ffffff', endColorstr='#979797',GradientType=1 )"
            : props.tool === "MGW" && props.color === "#fc7474"
            ? "progid:DXImageTransform.Microsoft.gradient( startColorstr='#fc7474', endColorstr='#ab3636',GradientType=1 )"
            : "progid:DXImageTransform.Microsoft.gradient( startColorstr='#92e9ba', endColorstr='#2d9e61',GradientType=1 )"}; /* IE6-9 */

    display: inline-block;
`;

const Bottom = styled.span`
    height: 15px;
    width: 30px;
    border-radius: 0 0 30px 30px;
    /* Permalink - use to edit and share this gradient: https://colorzilla.com/gradient-editor/#ffffff+0,ebebeb+50,979797+100 */
    /* Permalink - use to edit and share this gradient: https://colorzilla.com/gradient-editor/#8c8c8c+0,030303+93 */
    /* Permalink - use to edit and share this gradient: https://colorzilla.com/gradient-editor/#ffffff+0,ebebeb+50,979797+100 */
    background: ${(props) =>
        props.tool === "SHW" && props.color === "#2d9e61"
            ? "rgb(255, 255, 255)"
            : props.tool === "SHW" && props.color === "#979797"
            ? "rgb(140, 140, 140)"
            : props.tool === "MGW" && props.color === "#fc7474"
            ? "rgb(255,255,255)"
            : "rgb(0, 0, 0)"}; /* Old browsers */
    background: ${(props) =>
        props.tool === "SHW" && props.color === "#2d9e61"
            ? "-moz-linear-gradient(left,rgba(255, 255, 255, 1) 0%,rgba(235, 235, 235, 1) 50%,rgba(151, 151, 151, 1) 100%)"
            : props.tool === "SHW" && props.color === "#979797"
            ? "-moz-linear-gradient(left,rgba(140, 140, 140, 1) 0%,rgba(3, 3, 3, 1) 93%)"
            : props.tool === "MGW" && props.color === "#fc7474"
            ? "-moz-linear-gradient(left,  rgba(255,255,255,1) 0%, rgba(235,235,235,1) 50%, rgba(151,151,151,1) 100%)"
            : "rgb(0, 0, 0)"}; /* FF3.6-15 */
    background: ${(props) =>
        props.tool === "SHW" && props.color === "#2d9e61"
            ? "-webkit-linear-gradient(left,rgba(255, 255, 255, 1) 0%,rgba(235, 235, 235, 1) 50%,rgba(151, 151, 151, 1) 100%)"
            : props.tool === "SHW" && props.color === "#979797"
            ? "-webkit-linear-gradient(left,rgba(140, 140, 140, 1) 0%,rgba(3, 3, 3, 1) 93%)"
            : props.tool === "MGW" && props.color === "#fc7474"
            ? "-webkit-linear-gradient(left,  rgba(255,255,255,1) 0%,rgba(235,235,235,1) 50%,rgba(151,151,151,1) 100%)"
            : "rgb(0, 0, 0)"}; /* Chrome10-25,Safari5.1-6 */
    background: ${(props) =>
        props.tool === "SHW" && props.color === "#2d9e61"
            ? "linear-gradient(to right,rgba(255, 255, 255, 1) 0%,rgba(235, 235, 235, 1) 50%,rgba(151, 151, 151, 1) 100%)"
            : props.tool === "SHW" && props.color === "#979797"
            ? "linear-gradient(to right,rgba(140, 140, 140, 1) 0%,rgba(3, 3, 3, 1) 93%)"
            : props.tool === "MGW" && props.color === "#fc7474"
            ? "linear-gradient(to right,  rgba(255,255,255,1) 0%,rgba(235,235,235,1) 50%,rgba(151,151,151,1) 100%)"
            : "rgb(0, 0, 0)"}; /* W3C, IE10+, FF16+, Chrome26+, Opera12+, Safari7+ */
    filter: ${(props) =>
        props.tool === "SHW" && props.color === "#2d9e61"
            ? "progid:DXImageTransform.Microsoft.gradient( startColorstr='#ffffff', endColorstr='#979797',GradientType=1 )"
            : props.tool === "SHW" && props.color === "#979797"
            ? "progid:DXImageTransform.Microsoft.gradient( startColorstr='#8c8c8c', endColorstr='#030303',GradientType=1 )"
            : props.tool === "MGW" && props.color === "#fc7474"
            ? "progid:DXImageTransform.Microsoft.gradient( startColorstr='#ffffff', endColorstr='#979797',GradientType=1 )"
            : "progid:DXImageTransform.Microsoft.gradient( startColorstr='#ffffff', endColorstr='#979797',GradientType=1 )"}; /* IE6-9 */

    display: inline-block;
`;

const ColorDot = ({ className, active, onClick, single, ...props }) => (
    <Dot
        // style={{
        //   backgroundColor: color,
        //   border: active ? "2px solid #ACACAC" : "none",
        // }}
        onClick={onClick}
    >
        {single ? (
            <SingleDot
                style={{
                    border: active ? "2px solid black" : "none",
                }}
                {...props}
            />
        ) : (
            <>
                <Top
                    style={{
                        borderRight: active ? "2px solid black" : "none",
                        borderLeft: active ? "2px solid black" : "none",
                        borderTop: active ? "2px solid black" : "none",
                        borderBottom: 0,
                    }}
                    {...props}
                />
                <Bottom
                    style={{
                        borderRight: active ? "2px solid black" : "none",
                        borderLeft: active ? "2px solid black" : "none",
                        borderBottom: active ? "2px solid black" : "none",
                        borderTop: 0,
                    }}
                    {...props}
                />
            </>
        )}
    </Dot>
);

export default ColorDot;
