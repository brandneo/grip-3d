import React from "react";
import styled from "styled-components";

const StyledRow = styled.div`
    display: flex;
    flex-direction: row;

    @media (max-width: 576px) {
        justify-content: space-between;
        min-width: 90px;
    }
`;

const Row = ({ children, style, ...props }) => <StyledRow style={style}>{children}</StyledRow>;

export default Row;
