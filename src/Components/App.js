//DEPENDENCIES
import React, { Suspense, useState } from "react";
import { ThemeProvider } from "styled-components";
import { Canvas } from "react-three-fiber";
import * as THREE from "three";
import ReactTooltip from "react-tooltip";

//COMPONENTS
import GlobalStyles from "../Styles/GlobalStyle";
import Theme from "../Styles/Theme";
import Controls from "./Controls";
import Checkbox from "./ViewComponents/Checkbox";
import Dot from "./ViewComponents/Dot";
import Row from "./ViewComponents/Row";
import Overlay from "./ViewComponents/Overlay";
import Button from "./ViewComponents/Button";
import Image360 from "./ViewComponents/360";

import Grip from "./GraphicalComponents/Grip";
import Model from "./GraphicalComponents/Model";
import SHW from "./GraphicalComponents/SHW_Test";
import MGW from "./GraphicalComponents/MGW";
import Cinema from "./GraphicalComponents/Cinema";
import Brandneo from "./GraphicalComponents/Brandneo";

const App = () => {
    const [colorSet, setColorSet] = useState([
        { color: "#2d9e61", active: true, logo: "#d3d0c2", single: false, tool: "SHW" },
        { color: "#979797", active: false, logo: "#4a4a4a", single: false, tool: "SHW" },
        { color: "#030303", active: true, logo: "#d3d0c2", single: true, tool: "MGW" },
        { color: "#fc7474", active: false, logo: "#4a4a4a", single: false, tool: "MGW" },
    ]);
    const [activeTool, setActiveTool] = useState(window.app.activeToolType);
    const [color, setColor] = useState(activeTool === "SHW" ? { lever: "#2d9e61", logo: "#d3d0c2" } : { lever: "#030303", logo: "#d3d0c2" });
    const [show, setShow] = useState(true);
    const [options, SetOptions] = useState(window.app.activeLang === "de" ? [
        {
            name: "MEK-Option",
            checked: false,
            tooltipText: "Zusatzinformationen stehen hier",
        },
        {
            name: "Zentrierscheibe",
            checked: false,
            tooltipText: "Zusatzinformationen stehen hier",
        },
    ] : [
        {
            name: "MEK-options",
            checked: false,
            tooltipText: "Additional information is available here",
        },
        {
            name: "Centering disk",
            checked: false,
            tooltipText: "Additional information is available here",
        },
    ]);
    console.log("React App State",window.app)
    const [visibleObjects, setVisibleObjects] = useState([
        { name: "form0", check: true },
        { name: "form1", check: true },
        { name: "form2", check: true },
        { name: "form3", check: true },
        { name: "form4", check: true },
        { name: "form5", check: true },
        { name: "form6", check: true },
        { name: "form7", check: true },
        { name: "form8", check: true },
        { name: "form9", check: true },
        { name: "form10", check: true },
        { name: "form11", check: true },
        { name: "form12", check: true },
        { name: "form13", check: true },
        { name: "form14", check: true },
        { name: "form15", check: true },
        { name: "form16", check: true },
        { name: "form17", check: true },
        { name: "form18", check: true },
        { name: "form19", check: true },
        { name: "form20", check: true },
        { name: "form21", check: true },
        { name: "Unterseite", check: true },
        { name: "form23", check: true },
        { name: "Hebel", check: true },
        { name: "Oberseite", check: true },
        { name: "Bolzenlasche", check: true },
        { name: "form27", check: true },
        { name: "form28", check: true },
        { name: "form29", check: true },
        { name: "form30", check: true },
        { name: "form31", check: true },
        { name: "form32", check: true },
        { name: "form33", check: true },
        { name: "form34", check: true },
        { name: "form35", check: true },
        { name: "form36", check: true },
        { name: "form37", check: true },
        { name: "form38", check: true },
        { name: "form39", check: true },
        { name: "logo", check: true },
    ]);

    function handleColorSwitch(activeColor) {
        setColor({ ...color, lever: activeColor.color, logo: activeColor.logo });
        setColorSet(
            colorSet.map((item) => {
                if (item === activeColor) return { ...activeColor, active: true };
                return { ...item, active: false };
            })
        );
    }

    return (
        <>
            <ThemeProvider theme={Theme}>
                <GlobalStyles />
                <Overlay>
                    {/* <div>
                        {visibleObjects.map((visibleObject, index) => (
                            <label key={index}>
                                <Checkbox
                                    checked={visibleObject.check}
                                    onChange={() =>
                                        setVisibleObjects(
                                            visibleObjects.map((object) => {
                                                if (object === visibleObject)
                                                    return {
                                                        ...visibleObject,
                                                        check: !visibleObject.check,
                                                    };
                                                return object;
                                            })
                                        )
                                    }
                                />
                                <span style={{ marginLeft: 8 }}>{visibleObject.name}</span>
                                <br />
                            </label>
                        ))}
                    </div> */}
                    <br />
                    {/* <Row>
                        <Button onClick={() => (setActiveTool("SHW"), handleColorSwitch(colorSet[0]))}>SHW</Button>
                        <Button onClick={() => (setActiveTool("MGW"), handleColorSwitch(colorSet[2]))}>MGW</Button>
                    </Row> */}
                    <Row>
                        {colorSet
                            .filter((color) => color.tool === activeTool)
                            .map((item, index) => (
                                <Dot
                                    single={item.single}
                                    color={item.color}
                                    active={item.active}
                                    tool={item.tool}
                                    key={index}
                                    onClick={() => handleColorSwitch(item)}
                                />
                            ))}
                        {options.map((option, index) => (
                            <label key={index} className="options">
                                <Checkbox
                                    checked={option.checked}
                                    onChange={() =>
                                        SetOptions(
                                            options.map((obj) => {
                                                if (obj === option)
                                                    return {
                                                        ...option,
                                                        checked: !option.checked,
                                                    };
                                                return obj;
                                            })
                                        )
                                    }
                                />
                                <span style={{ marginLeft: 8, width: "95px" }} data-tip={option.tooltipText}>
                                    {option.name}
                                </span>
                            </label>
                        ))}
                    </Row>
                    <Image360 />
                    <ReactTooltip
                        type="light"
                        border={true}
                        borderColor="black"
                        textColor="black"
                        className="tooltip"
                    />
                </Overlay>

                <Canvas
                    camera={{ position: [0, 0, 50] }}
                    onCreated={({ gl, scene }) => {
                        gl.shadowMap.enabled = true;
                        gl.shadowMap.type = THREE.PCFSoftShadowMap;
                        scene.background = new THREE.Color("#ffffff");
                    }}
                >
                    <Controls />

                    <Suspense fallback={null}>
                        {/* <ambientLight intensity={0.001} /> */}
                        <directionalLight intensity={0.9} position={[0, 0, 200]} />
                        <directionalLight intensity={0.9} position={[200, 0, 0]} />
                        <directionalLight intensity={0.9} position={[0, 200, 0]} />
                        <directionalLight intensity={0.9} position={[0, 0, -200]} />
                        <directionalLight intensity={0.9} position={[-200, 0, 0]} />
                        <directionalLight intensity={0.9} position={[0, -200, 0]} />

                        {/* <Grip color={color} visibleObjects={visibleObjects} /> */}
                        {activeTool === "SHW" ? (
                            <Model color={color} visibleObjects={visibleObjects} options={options} />
                        ) : (
                            <MGW color={color} options={options} />
                        )}
                        {/* <SHW /> */}
                        {/* <Brandneo /> */}
                        {/* <Cinema /> */}
                    </Suspense>
                </Canvas>
            </ThemeProvider>
        </>
    );
};

export default App;
