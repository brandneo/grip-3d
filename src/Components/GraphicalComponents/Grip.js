import { GLTFLoader } from "three/examples/jsm/loaders/GLTFLoader";
import { useLoader } from "react-three-fiber";
import React, { useState } from "react";

const Custom = (props) => {
    // const gltf = useLoader(GLTFLoader, "/models/SHW_Custom_1.gltf");
    const testgltf = useLoader(GLTFLoader, "/models/GTLF-TEST_WAYS.gltf");
    // const gltf1 = useLoader(GLTFLoader, "/models/custom.gltf");
    const gltfSHW = useLoader(GLTFLoader, "/models/SHW_Custom.gltf");

    console.log("Custom Test", testgltf.__$, gltfSHW.__$);
    const [material, setMaterial] = useState(gltfSHW.__$[26].material.color);

    // gltf1.__$[6].material.color.r = 0;
    const { color, visibleObjects } = props;
    console.log("hebel", material, color, visibleObjects);

    gltfSHW.__$.forEach((element) => {
        if (element.material) {
            element.material.metalness = 1;
            element.material.roughness = 0.7;
            element.material.clearcoat = 0.4;
            element.material.clearcoatRoughness = 0.12;
        }
    });

    gltfSHW.__$[26].material.color = color;
    // gltfSHW.__$[42].material.color = gltf.__$[49].material.color;

    // gltf1.__$[6].material.roughness = 0.7;
    // gltf1.__$[6].material.clearcoat = 0.4;

    return (
        <group>
            {visibleObjects[0].check && (
                <mesh
                    receiveShadow
                    onPointerOver={(e) => {
                        //I want to change colors of vectors that i hover!
                        //console.log(e.point)
                    }}
                    onPointerDown={(e) => {
                        e.stopPropagation();

                        if (e.point.length() > 80) {
                            console.log(e.point);
                            //here we will plant some trees in the future
                        }
                    }}
                    scale={[0.9, 0.9, 0.9]}
                    position={[0, 0, 0]}
                >
                    <bufferGeometry attach="geometry" {...gltfSHW.__$[2].geometry} />
                    <meshPhysicalMaterial attach="material" {...gltfSHW.__$[2].material} />
                </mesh>
            )}
            {visibleObjects[1].check && (
                <mesh
                    receiveShadow
                    onPointerOver={(e) => {
                        //I want to change colors of vectors that i hover!
                        //console.log(e.point)
                    }}
                    onPointerDown={(e) => {
                        e.stopPropagation();

                        if (e.point.length() > 80) {
                            console.log(e.point);
                            //here we will plant some trees in the future
                        }
                    }}
                    scale={[0.9, 0.9, 0.9]}
                    position={[0, 0, 0]}
                >
                    <bufferGeometry attach="geometry" {...gltfSHW.__$[3].geometry} />
                    <meshPhysicalMaterial attach="material" {...gltfSHW.__$[3].material} />
                </mesh>
            )}
            {visibleObjects[2].check && (
                <mesh
                    receiveShadow
                    onPointerOver={(e) => {
                        //I want to change colors of vectors that i hover!
                        //console.log(e.point)
                    }}
                    onPointerDown={(e) => {
                        e.stopPropagation();

                        if (e.point.length() > 80) {
                            console.log(e.point);
                            //here we will plant some trees in the future
                        }
                    }}
                    scale={[0.9, 0.9, 0.9]}
                    position={[0, 0, 0]}
                >
                    <bufferGeometry attach="geometry" {...gltfSHW.__$[4].geometry} />
                    <meshPhysicalMaterial attach="material" {...gltfSHW.__$[4].material} />
                </mesh>
            )}
            {visibleObjects[3].check && (
                <mesh
                    receiveShadow
                    onPointerOver={(e) => {
                        //I want to change colors of vectors that i hover!
                        //console.log(e.point)
                    }}
                    onPointerDown={(e) => {
                        e.stopPropagation();

                        if (e.point.length() > 80) {
                            console.log(e.point);
                            //here we will plant some trees in the future
                        }
                    }}
                    scale={[0.9, 0.9, 0.9]}
                    position={[0, 0, 0]}
                >
                    <bufferGeometry attach="geometry" {...gltfSHW.__$[5].geometry} />
                    <meshPhysicalMaterial attach="material" {...gltfSHW.__$[5].material} />
                </mesh>
            )}
            {visibleObjects[4].check && (
                <mesh
                    receiveShadow
                    onPointerOver={(e) => {
                        //I want to change colors of vectors that i hover!
                        //console.log(e.point)
                    }}
                    onPointerDown={(e) => {
                        e.stopPropagation();

                        if (e.point.length() > 80) {
                            console.log(e.point);
                            //here we will plant some trees in the future
                        }
                    }}
                    scale={[0.9, 0.9, 0.9]}
                    position={[0, 0, 0]}
                >
                    <bufferGeometry attach="geometry" {...gltfSHW.__$[6].geometry} />
                    <meshPhysicalMaterial attach="material" {...gltfSHW.__$[6].material} />
                </mesh>
            )}
            {visibleObjects[5].check && (
                <mesh
                    receiveShadow
                    onPointerOver={(e) => {
                        //I want to change colors of vectors that i hover!
                        //console.log(e.point)
                    }}
                    onPointerDown={(e) => {
                        e.stopPropagation();

                        if (e.point.length() > 80) {
                            console.log(e.point);
                            //here we will plant some trees in the future
                        }
                    }}
                    scale={[0.9, 0.9, 0.9]}
                    position={[0, 0, 0]}
                >
                    <bufferGeometry attach="geometry" {...gltfSHW.__$[7].geometry} />
                    <meshPhysicalMaterial attach="material" {...gltfSHW.__$[7].material} />
                </mesh>
            )}
            {visibleObjects[6].check && (
                <mesh
                    receiveShadow
                    onPointerOver={(e) => {
                        //I want to change colors of vectors that i hover!
                        //console.log(e.point)
                    }}
                    onPointerDown={(e) => {
                        e.stopPropagation();

                        if (e.point.length() > 80) {
                            console.log(e.point);
                            //here we will plant some trees in the future
                        }
                    }}
                    scale={[0.9, 0.9, 0.9]}
                    position={[0, 0, 0]}
                >
                    <bufferGeometry attach="geometry" {...gltfSHW.__$[8].geometry} />
                    <meshPhysicalMaterial attach="material" {...gltfSHW.__$[8].material} />
                </mesh>
            )}
            {visibleObjects[7].check && (
                <mesh
                    receiveShadow
                    onPointerOver={(e) => {
                        //I want to change colors of vectors that i hover!
                        //console.log(e.point)
                    }}
                    onPointerDown={(e) => {
                        e.stopPropagation();

                        if (e.point.length() > 80) {
                            console.log(e.point);
                            //here we will plant some trees in the future
                        }
                    }}
                    scale={[0.9, 0.9, 0.9]}
                    position={[0, 0, 0]}
                >
                    <bufferGeometry attach="geometry" {...gltfSHW.__$[9].geometry} />
                    <meshPhysicalMaterial attach="material" {...gltfSHW.__$[9].material} />
                </mesh>
            )}
            {visibleObjects[8].check && (
                <mesh
                    receiveShadow
                    onPointerOver={(e) => {
                        //I want to change colors of vectors that i hover!
                        //console.log(e.point)
                    }}
                    onPointerDown={(e) => {
                        e.stopPropagation();

                        if (e.point.length() > 80) {
                            console.log(e.point);
                            //here we will plant some trees in the future
                        }
                    }}
                    scale={[0.9, 0.9, 0.9]}
                    position={[0, 0, 0]}
                >
                    <bufferGeometry attach="geometry" {...gltfSHW.__$[10].geometry} />
                    <meshPhysicalMaterial attach="material" {...gltfSHW.__$[10].material} />
                </mesh>
            )}
            {visibleObjects[9].check && (
                <mesh
                    receiveShadow
                    onPointerOver={(e) => {
                        //I want to change colors of vectors that i hover!
                        //console.log(e.point)
                    }}
                    onPointerDown={(e) => {
                        e.stopPropagation();

                        if (e.point.length() > 80) {
                            console.log(e.point);
                            //here we will plant some trees in the future
                        }
                    }}
                    scale={[0.9, 0.9, 0.9]}
                    position={[0, 0, 0]}
                >
                    <bufferGeometry attach="geometry" {...gltfSHW.__$[11].geometry} />
                    <meshPhysicalMaterial attach="material" {...gltfSHW.__$[11].material} />
                </mesh>
            )}
            {visibleObjects[10].check && (
                <mesh
                    receiveShadow
                    onPointerOver={(e) => {
                        //I want to change colors of vectors that i hover!
                        //console.log(e.point)
                    }}
                    onPointerDown={(e) => {
                        e.stopPropagation();

                        if (e.point.length() > 80) {
                            console.log(e.point);
                            //here we will plant some trees in the future
                        }
                    }}
                    scale={[0.9, 0.9, 0.9]}
                    position={[0, 0, 0]}
                >
                    <bufferGeometry attach="geometry" {...gltfSHW.__$[12].geometry} />
                    <meshPhysicalMaterial attach="material" {...gltfSHW.__$[12].material} />
                </mesh>
            )}
            {visibleObjects[11].check && (
                <mesh
                    receiveShadow
                    onPointerOver={(e) => {
                        //I want to change colors of vectors that i hover!
                        //console.log(e.point)
                    }}
                    onPointerDown={(e) => {
                        e.stopPropagation();

                        if (e.point.length() > 80) {
                            console.log(e.point);
                            //here we will plant some trees in the future
                        }
                    }}
                    scale={[0.9, 0.9, 0.9]}
                    position={[0, 0, 0]}
                >
                    <bufferGeometry attach="geometry" {...gltfSHW.__$[13].geometry} />
                    <meshPhysicalMaterial attach="material" {...gltfSHW.__$[13].material} />
                </mesh>
            )}
            {visibleObjects[12].check && (
                <mesh
                    receiveShadow
                    onPointerOver={(e) => {
                        //I want to change colors of vectors that i hover!
                        //console.log(e.point)
                    }}
                    onPointerDown={(e) => {
                        e.stopPropagation();

                        if (e.point.length() > 80) {
                            console.log(e.point);
                            //here we will plant some trees in the future
                        }
                    }}
                    scale={[0.9, 0.9, 0.9]}
                    position={[0, 0, 0]}
                >
                    <bufferGeometry attach="geometry" {...gltfSHW.__$[14].geometry} />
                    <meshPhysicalMaterial attach="material" {...gltfSHW.__$[14].material} />
                </mesh>
            )}
            {visibleObjects[13].check && (
                <mesh
                    receiveShadow
                    onPointerOver={(e) => {
                        //I want to change colors of vectors that i hover!
                        //console.log(e.point)
                    }}
                    onPointerDown={(e) => {
                        e.stopPropagation();

                        if (e.point.length() > 80) {
                            console.log(e.point);
                            //here we will plant some trees in the future
                        }
                    }}
                    scale={[0.9, 0.9, 0.9]}
                    position={[0, 0, 0]}
                >
                    <bufferGeometry attach="geometry" {...gltfSHW.__$[15].geometry} />
                    <meshPhysicalMaterial attach="material" {...gltfSHW.__$[15].material} />
                </mesh>
            )}
            {visibleObjects[14].check && (
                <mesh
                    receiveShadow
                    onPointerOver={(e) => {
                        //I want to change colors of vectors that i hover!
                        //console.log(e.point)
                    }}
                    onPointerDown={(e) => {
                        e.stopPropagation();

                        if (e.point.length() > 80) {
                            console.log(e.point);
                            //here we will plant some trees in the future
                        }
                    }}
                    scale={[0.9, 0.9, 0.9]}
                    position={[0, 0, 0]}
                >
                    <bufferGeometry attach="geometry" {...gltfSHW.__$[16].geometry} />
                    <meshPhysicalMaterial attach="material" {...gltfSHW.__$[16].material} />
                </mesh>
            )}
            {visibleObjects[15].check && (
                <mesh
                    receiveShadow
                    onPointerOver={(e) => {
                        //I want to change colors of vectors that i hover!
                        //console.log(e.point)
                    }}
                    onPointerDown={(e) => {
                        e.stopPropagation();

                        if (e.point.length() > 80) {
                            console.log(e.point);
                            //here we will plant some trees in the future
                        }
                    }}
                    scale={[0.9, 0.9, 0.9]}
                    position={[0, 0, 0]}
                >
                    <bufferGeometry attach="geometry" {...gltfSHW.__$[17].geometry} />
                    <meshPhysicalMaterial attach="material" {...gltfSHW.__$[17].material} />
                </mesh>
            )}
            {visibleObjects[16].check && (
                <mesh
                    receiveShadow
                    onPointerOver={(e) => {
                        //I want to change colors of vectors that i hover!
                        //console.log(e.point)
                    }}
                    onPointerDown={(e) => {
                        e.stopPropagation();

                        if (e.point.length() > 80) {
                            console.log(e.point);
                            //here we will plant some trees in the future
                        }
                    }}
                    scale={[0.9, 0.9, 0.9]}
                    position={[0, 0, 0]}
                >
                    <bufferGeometry attach="geometry" {...gltfSHW.__$[18].geometry} />
                    <meshPhysicalMaterial attach="material" {...gltfSHW.__$[18].material} />
                </mesh>
            )}
            {visibleObjects[17].check && (
                <mesh
                    receiveShadow
                    onPointerOver={(e) => {
                        //I want to change colors of vectors that i hover!
                        //console.log(e.point)
                    }}
                    onPointerDown={(e) => {
                        e.stopPropagation();

                        if (e.point.length() > 80) {
                            console.log(e.point);
                            //here we will plant some trees in the future
                        }
                    }}
                    scale={[0.9, 0.9, 0.9]}
                    position={[0, 0, 0]}
                >
                    <bufferGeometry attach="geometry" {...gltfSHW.__$[19].geometry} />
                    <meshPhysicalMaterial attach="material" {...gltfSHW.__$[19].material} />
                </mesh>
            )}
            {visibleObjects[18].check && (
                <mesh
                    receiveShadow
                    onPointerOver={(e) => {
                        //I want to change colors of vectors that i hover!
                        //console.log(e.point)
                    }}
                    onPointerDown={(e) => {
                        e.stopPropagation();

                        if (e.point.length() > 80) {
                            console.log(e.point);
                            //here we will plant some trees in the future
                        }
                    }}
                    scale={[0.9, 0.9, 0.9]}
                    position={[0, 0, 0]}
                >
                    <bufferGeometry attach="geometry" {...gltfSHW.__$[20].geometry} />
                    <meshPhysicalMaterial attach="material" {...gltfSHW.__$[20].material} />
                </mesh>
            )}
            {visibleObjects[19].check && (
                <mesh
                    receiveShadow
                    onPointerOver={(e) => {
                        //I want to change colors of vectors that i hover!
                        //console.log(e.point)
                    }}
                    onPointerDown={(e) => {
                        e.stopPropagation();

                        if (e.point.length() > 80) {
                            console.log(e.point);
                            //here we will plant some trees in the future
                        }
                    }}
                    scale={[0.9, 0.9, 0.9]}
                    position={[0, 0, 0]}
                >
                    <bufferGeometry attach="geometry" {...gltfSHW.__$[21].geometry} />
                    <meshPhysicalMaterial attach="material" {...gltfSHW.__$[21].material} />
                </mesh>
            )}
            {visibleObjects[20].check && (
                <mesh
                    receiveShadow
                    onPointerOver={(e) => {
                        //I want to change colors of vectors that i hover!
                        //console.log(e.point)
                    }}
                    onPointerDown={(e) => {
                        e.stopPropagation();

                        if (e.point.length() > 80) {
                            console.log(e.point);
                            //here we will plant some trees in the future
                        }
                    }}
                    scale={[0.9, 0.9, 0.9]}
                    position={[0, 0, 0]}
                >
                    <bufferGeometry attach="geometry" {...gltfSHW.__$[22].geometry} />
                    <meshPhysicalMaterial attach="material" {...gltfSHW.__$[22].material} />
                </mesh>
            )}
            {visibleObjects[21].check && (
                <mesh
                    receiveShadow
                    onPointerOver={(e) => {
                        //I want to change colors of vectors that i hover!
                        //console.log(e.point)
                    }}
                    onPointerDown={(e) => {
                        e.stopPropagation();

                        if (e.point.length() > 80) {
                            console.log(e.point);
                            //here we will plant some trees in the future
                        }
                    }}
                    scale={[0.9, 0.9, 0.9]}
                    position={[0, 0, 0]}
                >
                    <bufferGeometry attach="geometry" {...gltfSHW.__$[23].geometry} />
                    <meshPhysicalMaterial attach="material" {...gltfSHW.__$[23].material} />
                </mesh>
            )}
            {visibleObjects[22].check && (
                <mesh
                    receiveShadow
                    onPointerOver={(e) => {
                        //I want to change colors of vectors that i hover!
                        //console.log(e.point)
                    }}
                    onPointerDown={(e) => {
                        e.stopPropagation();

                        if (e.point.length() > 80) {
                            console.log(e.point);
                            //here we will plant some trees in the future
                        }
                    }}
                    scale={[0.9, 0.9, 0.9]}
                    position={[0, 0, 0]}
                >
                    <bufferGeometry attach="geometry" {...gltfSHW.__$[24].geometry} />
                    <meshPhysicalMaterial attach="material" {...gltfSHW.__$[24].material} />
                </mesh>
            )}
            {visibleObjects[23].check && (
                <mesh
                    receiveShadow
                    onPointerOver={(e) => {
                        //I want to change colors of vectors that i hover!
                        //console.log(e.point)
                    }}
                    onPointerDown={(e) => {
                        e.stopPropagation();

                        if (e.point.length() > 80) {
                            console.log(e.point);
                            //here we will plant some trees in the future
                        }
                    }}
                    scale={[0.9, 0.9, 0.9]}
                    position={[0, 0, 0]}
                >
                    <bufferGeometry attach="geometry" {...gltfSHW.__$[25].geometry} />
                    <meshPhysicalMaterial attach="material" {...gltfSHW.__$[25].material} />
                </mesh>
            )}
            {visibleObjects[24].check && (
                <mesh
                    receiveShadow
                    onPointerOver={(e) => {
                        //I want to change colors of vectors that i hover!
                        //console.log(e.point)
                    }}
                    onPointerDown={(e) => {
                        e.stopPropagation();

                        if (e.point.length() > 80) {
                            console.log(e.point);
                            //here we will plant some trees in the future
                        }
                    }}
                    scale={[0.9, 0.9, 0.9]}
                    position={[0, 0, 0]}
                >
                    <bufferGeometry attach="geometry" {...gltfSHW.__$[26].geometry} />
                    <meshPhysicalMaterial attach="material" {...gltfSHW.__$[26].material} />
                </mesh>
            )}
            {visibleObjects[25].check && (
                <mesh
                    receiveShadow
                    onPointerOver={(e) => {
                        //I want to change colors of vectors that i hover!
                        //console.log(e.point)
                    }}
                    onPointerDown={(e) => {
                        e.stopPropagation();

                        if (e.point.length() > 80) {
                            console.log(e.point);
                            //here we will plant some trees in the future
                        }
                    }}
                    scale={[0.9, 0.9, 0.9]}
                    position={[0, 0, 0]}
                >
                    <bufferGeometry attach="geometry" {...gltfSHW.__$[27].geometry} />
                    <meshPhysicalMaterial attach="material" {...gltfSHW.__$[27].material} />
                </mesh>
            )}
            {visibleObjects[26].check && (
                <mesh
                    receiveShadow
                    onPointerOver={(e) => {
                        //I want to change colors of vectors that i hover!
                        //console.log(e.point)
                    }}
                    onPointerDown={(e) => {
                        e.stopPropagation();

                        if (e.point.length() > 80) {
                            console.log(e.point);
                            //here we will plant some trees in the future
                        }
                    }}
                    scale={[0.9, 0.9, 0.9]}
                    position={[0, 0, 0]}
                >
                    <bufferGeometry attach="geometry" {...gltfSHW.__$[28].geometry} />
                    <meshPhysicalMaterial attach="material" {...gltfSHW.__$[28].material} />
                </mesh>
            )}
            {visibleObjects[27].check && (
                <mesh
                    receiveShadow
                    onPointerOver={(e) => {
                        //I want to change colors of vectors that i hover!
                        //console.log(e.point)
                    }}
                    onPointerDown={(e) => {
                        e.stopPropagation();

                        if (e.point.length() > 80) {
                            console.log(e.point);
                            //here we will plant some trees in the future
                        }
                    }}
                    scale={[0.9, 0.9, 0.9]}
                    position={[0, 0, 0]}
                >
                    <bufferGeometry attach="geometry" {...gltfSHW.__$[29].geometry} />
                    <meshPhysicalMaterial attach="material" {...gltfSHW.__$[29].material} />
                </mesh>
            )}
            {visibleObjects[28].check && (
                <mesh
                    receiveShadow
                    onPointerOver={(e) => {
                        //I want to change colors of vectors that i hover!
                        //console.log(e.point)
                    }}
                    onPointerDown={(e) => {
                        e.stopPropagation();

                        if (e.point.length() > 80) {
                            console.log(e.point);
                            //here we will plant some trees in the future
                        }
                    }}
                    scale={[0.9, 0.9, 0.9]}
                    position={[0, 0, 0]}
                >
                    <bufferGeometry attach="geometry" {...gltfSHW.__$[30].geometry} />
                    <meshPhysicalMaterial attach="material" {...gltfSHW.__$[30].material} />
                </mesh>
            )}
            {visibleObjects[29].check && (
                <mesh
                    receiveShadow
                    onPointerOver={(e) => {
                        //I want to change colors of vectors that i hover!
                        //console.log(e.point)
                    }}
                    onPointerDown={(e) => {
                        e.stopPropagation();

                        if (e.point.length() > 80) {
                            console.log(e.point);
                            //here we will plant some trees in the future
                        }
                    }}
                    scale={[0.9, 0.9, 0.9]}
                    position={[0, 0, 0]}
                >
                    <bufferGeometry attach="geometry" {...gltfSHW.__$[31].geometry} />
                    <meshPhysicalMaterial attach="material" {...gltfSHW.__$[31].material} />
                </mesh>
            )}
            {visibleObjects[30].check && (
                <mesh
                    receiveShadow
                    onPointerOver={(e) => {
                        //I want to change colors of vectors that i hover!
                        //console.log(e.point)
                    }}
                    onPointerDown={(e) => {
                        e.stopPropagation();

                        if (e.point.length() > 80) {
                            console.log(e.point);
                            //here we will plant some trees in the future
                        }
                    }}
                    scale={[0.9, 0.9, 0.9]}
                    position={[0, 0, 0]}
                >
                    <bufferGeometry attach="geometry" {...gltfSHW.__$[32].geometry} />
                    <meshPhysicalMaterial attach="material" {...gltfSHW.__$[32].material} />
                </mesh>
            )}
            {visibleObjects[31].check && (
                <mesh
                    receiveShadow
                    onPointerOver={(e) => {
                        //I want to change colors of vectors that i hover!
                        //console.log(e.point)
                    }}
                    onPointerDown={(e) => {
                        e.stopPropagation();

                        if (e.point.length() > 80) {
                            console.log(e.point);
                            //here we will plant some trees in the future
                        }
                    }}
                    scale={[0.9, 0.9, 0.9]}
                    position={[0, 0, 0]}
                >
                    <bufferGeometry attach="geometry" {...gltfSHW.__$[33].geometry} />
                    <meshPhysicalMaterial attach="material" {...gltfSHW.__$[33].material} />
                </mesh>
            )}
            {visibleObjects[32].check && (
                <mesh
                    receiveShadow
                    onPointerOver={(e) => {
                        //I want to change colors of vectors that i hover!
                        //console.log(e.point)
                    }}
                    onPointerDown={(e) => {
                        e.stopPropagation();

                        if (e.point.length() > 80) {
                            console.log(e.point);
                            //here we will plant some trees in the future
                        }
                    }}
                    scale={[0.9, 0.9, 0.9]}
                    position={[0, 0, 0]}
                >
                    <bufferGeometry attach="geometry" {...gltfSHW.__$[34].geometry} />
                    <meshPhysicalMaterial attach="material" {...gltfSHW.__$[34].material} />
                </mesh>
            )}
            {visibleObjects[33].check && (
                <mesh
                    receiveShadow
                    onPointerOver={(e) => {
                        //I want to change colors of vectors that i hover!
                        //console.log(e.point)
                    }}
                    onPointerDown={(e) => {
                        e.stopPropagation();

                        if (e.point.length() > 80) {
                            console.log(e.point);
                            //here we will plant some trees in the future
                        }
                    }}
                    scale={[0.9, 0.9, 0.9]}
                    position={[0, 0, 0]}
                >
                    <bufferGeometry attach="geometry" {...gltfSHW.__$[35].geometry} />
                    <meshPhysicalMaterial attach="material" {...gltfSHW.__$[35].material} />
                </mesh>
            )}
            {visibleObjects[34].check && (
                <mesh
                    receiveShadow
                    onPointerOver={(e) => {
                        //I want to change colors of vectors that i hover!
                        //console.log(e.point)
                    }}
                    onPointerDown={(e) => {
                        e.stopPropagation();

                        if (e.point.length() > 80) {
                            console.log(e.point);
                            //here we will plant some trees in the future
                        }
                    }}
                    scale={[0.9, 0.9, 0.9]}
                    position={[0, 0, 0]}
                >
                    <bufferGeometry attach="geometry" {...gltfSHW.__$[36].geometry} />
                    <meshPhysicalMaterial attach="material" {...gltfSHW.__$[36].material} />
                </mesh>
            )}
            {visibleObjects[35].check && (
                <mesh
                    receiveShadow
                    onPointerOver={(e) => {
                        //I want to change colors of vectors that i hover!
                        //console.log(e.point)
                    }}
                    onPointerDown={(e) => {
                        e.stopPropagation();

                        if (e.point.length() > 80) {
                            console.log(e.point);
                            //here we will plant some trees in the future
                        }
                    }}
                    scale={[0.9, 0.9, 0.9]}
                    position={[0, 0, 0]}
                >
                    <bufferGeometry attach="geometry" {...gltfSHW.__$[37].geometry} />
                    <meshPhysicalMaterial attach="material" {...gltfSHW.__$[37].material} />
                </mesh>
            )}
            {visibleObjects[36].check && (
                <mesh
                    receiveShadow
                    onPointerOver={(e) => {
                        //I want to change colors of vectors that i hover!
                        //console.log(e.point)
                    }}
                    onPointerDown={(e) => {
                        e.stopPropagation();

                        if (e.point.length() > 80) {
                            console.log(e.point);
                            //here we will plant some trees in the future
                        }
                    }}
                    scale={[0.9, 0.9, 0.9]}
                    position={[0, 0, 0]}
                >
                    <bufferGeometry attach="geometry" {...gltfSHW.__$[38].geometry} />
                    <meshPhysicalMaterial attach="material" {...gltfSHW.__$[38].material} />
                </mesh>
            )}
            {visibleObjects[37].check && (
                <mesh
                    receiveShadow
                    onPointerOver={(e) => {
                        //I want to change colors of vectors that i hover!
                        //console.log(e.point)
                    }}
                    onPointerDown={(e) => {
                        e.stopPropagation();

                        if (e.point.length() > 80) {
                            console.log(e.point);
                            //here we will plant some trees in the future
                        }
                    }}
                    scale={[0.9, 0.9, 0.9]}
                    position={[0, 0, 0]}
                >
                    <bufferGeometry attach="geometry" {...gltfSHW.__$[39].geometry} />
                    <meshPhysicalMaterial attach="material" {...gltfSHW.__$[39].material} />
                </mesh>
            )}
            {visibleObjects[38].check && (
                <mesh
                    receiveShadow
                    onPointerOver={(e) => {
                        //I want to change colors of vectors that i hover!
                        //console.log(e.point)
                    }}
                    onPointerDown={(e) => {
                        e.stopPropagation();

                        if (e.point.length() > 80) {
                            console.log(e.point);
                            //here we will plant some trees in the future
                        }
                    }}
                    scale={[0.9, 0.9, 0.9]}
                    position={[0, 0, 0]}
                >
                    <bufferGeometry attach="geometry" {...gltfSHW.__$[40].geometry} />
                    <meshPhysicalMaterial attach="material" {...gltfSHW.__$[40].material} />
                </mesh>
            )}
            {visibleObjects[39].check && (
                <mesh
                    receiveShadow
                    onPointerOver={(e) => {
                        //I want to change colors of vectors that i hover!
                        //console.log(e.point)
                    }}
                    onPointerDown={(e) => {
                        e.stopPropagation();

                        if (e.point.length() > 80) {
                            console.log(e.point);
                            //here we will plant some trees in the future
                        }
                    }}
                    scale={[0.9, 0.9, 0.9]}
                    position={[0, 0, 0]}
                >
                    <bufferGeometry attach="geometry" {...gltfSHW.__$[41].geometry} />
                    <meshPhysicalMaterial attach="material" {...gltfSHW.__$[41].material} />
                </mesh>
            )}
            {visibleObjects[40].check && (
                <mesh
                    receiveShadow
                    onPointerOver={(e) => {
                        //I want to change colors of vectors that i hover!
                        //console.log(e.point)
                    }}
                    onPointerDown={(e) => {
                        e.stopPropagation();

                        if (e.point.length() > 80) {
                            console.log(e.point);
                            //here we will plant some trees in the future
                        }
                    }}
                    scale={[0.9, 0.9, 0.9]}
                    position={[0, 0, 0]}
                >
                    <bufferGeometry attach="geometry" {...gltfSHW.__$[42].geometry} />
                    <meshPhysicalMaterial attach="material" {...gltfSHW.__$[42].material} />
                </mesh>
            )}
        </group>
    );
};

export default Custom;
