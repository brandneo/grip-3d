import { createGlobalStyle } from "styled-components";

const GlobalStyles = createGlobalStyle`

*,*::before,*::after{
    padding: 0;
    margin:0;
    box-sizing: border-box;
    position: relative;
}
canvas{
    background: black;
}
#react{
    width: 100vw;
    height: 100vh;
    font-family: industry, sans-serif;
    font-style: normal;
    font-weight: 700;
    max-height: 300px;

    // Extra small devices (portrait phones, less than 576px)
    @media (min-width: 575.98px) { 
        max-height: 400px;
     }

    // Small devices (landscape phones, less than 768px)
    @media (min-width: 767.98px) { 
        max-height: 500px;
     }

    // Medium devices (tablets, less than 992px)
    @media (min-width: 991.98px) { 
        max-height: 600px;
     }

    // Large devices (desktops, less than 1200px)
    @media (min-width: 1199.98px) { 
        max-height: 600px;
     }
}

.tooltip {
    border-radius: 0;
    font-family: industry, sans-serif;
    font-style: normal;
    font-weight: 400;
}

.options {
    display: flex;
    flex-direction: row;
    margin: 6% 0; 
    @media (max-width: 576px) {
        display: none;
    }
}

span, p {
    font-size: 14px;
    line-height: 20px;
}
`;

export default GlobalStyles;
